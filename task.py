import math


def solution(a, b, c):
    result = (b * b) - (4 * a * c)
    if result < 0:
        print("None")
    elif result == 0:
        x = int((-b + math.sqrt(b * b - 4 * a * c)) / (2 * a))
        print("(" + str(x) + ",)")
    else:
        x = int((-b + math.sqrt(b * b - 4 * a * c)) / (2 * a))
        y = int((-b - math.sqrt(b * b - 4 * a * c)) / (2 * a))
        lista = [x, y]
        lista.sort()
        print("(" + str(lista[0]) + "," + str(lista[1]) + ")")